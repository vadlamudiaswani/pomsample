package org.testleaf.leaftaps.pages;

import org.testleaf.leaftaps.base.ProjectBase;

public class CreateLeadPage extends ProjectBase{

	public CreateLeadPage()
	{
		
	}
	public CreateLeadPage enterCompanyName()
	{
		driver.findElementById("createLeadForm_companyName").sendKeys("Test");
		
		return this;
	}
	
	public CreateLeadPage enterFirstName()
	{
		driver.findElementById("createLeadForm_firstName").sendKeys("firstname");
		firstName=driver.findElementById("createLeadForm_firstName").getText();
		return this;
	}
	public CreateLeadPage enterLastName()
	{
		driver.findElementById("createLeadForm_lastName").sendKeys("lastName");
		return this;
	}
	public ViewLeadPage clickSubmit()
	{
		driver.findElementByXPath("//input[@class='smallSubmit']").click();
		return new ViewLeadPage();
	}
}
