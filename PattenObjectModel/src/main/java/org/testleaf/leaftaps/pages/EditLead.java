package org.testleaf.leaftaps.pages;

import org.testleaf.leaftaps.base.ProjectBase;

public class EditLead extends ProjectBase {
	public EditLead() {

	}

	// Modify the company name value
	public EditLead editCompanyName() {
		driver.findElementById("updateLeadForm_companyName").clear();
		companyName="Cognizent";
		driver.findElementById("updateLeadForm_companyName").sendKeys(companyName);
		return this;

	}

	// Click on update button
	public ViewLeadPage clickUpdate() {
		driver.findElementByXPath("//input[@class='smallSubmit' and @value='Update']").click();
		return new ViewLeadPage();
	}

}
