package org.testleaf.leaftaps.pages;

import org.testleaf.leaftaps.base.ProjectBase;

public class FindLeads extends ProjectBase {
	public FindLeads()
	{
		
	}
	public ViewLeadPage clickFirstLeadID()
	{
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		return new ViewLeadPage();
	}

}
