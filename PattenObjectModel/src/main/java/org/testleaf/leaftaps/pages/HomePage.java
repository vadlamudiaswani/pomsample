package org.testleaf.leaftaps.pages;

import org.testleaf.leaftaps.base.ProjectBase;

public class HomePage extends ProjectBase{

	public HomePage() {

	}
	
	public HomePage verifyLoginName() {
		
		if(driver.findElementByTagName("h2").getText().contains("Demo sales manager")) {
			System.out.println("Login Verified");
		}
		else
		{
			System.out.println("Login Mismatch");
		}
		return this;
		
	}
	
	public MyHomePage clickcrmsfaLink()
	{
		driver.findElementByLinkText("CRM/SFA").click();
		return new MyHomePage();
	}
	
	public LoginPage logOut() {
		driver.findElementByXPath("//a[text()='Logout']").click();
		return new LoginPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
