package org.testleaf.leaftaps.pages;

import org.testleaf.leaftaps.base.ProjectBase;

public class MyLeadsPage extends ProjectBase{
	
	public MyLeadsPage() {
		
	}
	
	//Click CreateLead
	public CreateLeadPage clickCreateLead()
	{
		driver.findElementByXPath("//a[text()='Create Lead']").click();
		return new CreateLeadPage();
	}
	
	//Click FindLeads
	public FindLeads clickFindLeads()
	{
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		return new FindLeads();
	}
}
