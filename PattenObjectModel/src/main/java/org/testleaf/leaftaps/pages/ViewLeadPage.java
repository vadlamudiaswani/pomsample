package org.testleaf.leaftaps.pages;

import org.testleaf.leaftaps.base.ProjectBase;

public class ViewLeadPage extends ProjectBase {

	public ViewLeadPage verifyFirstName() {
		String name = driver.findElementById("viewLead_firstName_sp").getText();
		if (name.contains(firstName)) {
			System.out.println("First Name Verified");
		} else {
			System.out.println("First not Verified");
		}
		return this;
	}

	// Click on Edit button
	public EditLead clickEdit() {
		String previous_Company = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println("Previous Company Name is: " + previous_Company);
		driver.findElementByXPath("//a[text()='Edit']").click();
		return new EditLead();
	}

	// verifying Company name

	public ViewLeadPage verifyCompanyName() {
		String Name = driver.findElementById("viewLead_companyName_sp").getText();
		if (Name.substring(0, companyName.length()).equals(companyName)) {
			System.out.println("New Company Name is " + Name.substring(0, companyName.length())
					+ " Company name Matches : Update Successful");

		} else {
			System.out.println("Company Name not Matches Update Not Successful");
		}
		return this;
	}
}
