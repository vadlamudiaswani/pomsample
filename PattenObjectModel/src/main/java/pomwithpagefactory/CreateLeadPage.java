package pomwithpagefactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CreateLeadPage extends pomwithpagefactory.ProjectBase {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName")
	WebElement eleCompanyName;
	@FindBy(id = "createLeadForm_firstName")
	WebElement eleFirstName;
	@FindBy(id = "createLeadForm_lastName")
	WebElement eleLastName;
	@FindBy(how = How.XPATH, using = "//input[@class='smallSubmit']")
	WebElement elesubmit;

	public CreateLeadPage enterCompanyName() {
		enterData(eleCompanyName, "Cognizent");
		return this;
	}

	public CreateLeadPage enterFirstName() {
		eleFirstName.sendKeys("firstname");
		firstName = driver.findElementById("createLeadForm_firstName").getText();
		return this;
	}

	public CreateLeadPage enterLastName() {
		eleLastName.sendKeys("lastName");
		return this;
	}

	public ViewLeadPage clickSubmit() {
		elesubmit.click();
		return new ViewLeadPage();
	}
}
