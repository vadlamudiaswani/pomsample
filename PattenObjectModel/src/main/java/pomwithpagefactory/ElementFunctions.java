package pomwithpagefactory;

import org.openqa.selenium.WebElement;

public class ElementFunctions {
	
	public void enterData(WebElement ele,String data)
	{
		ele.clear();
		ele.sendKeys(data);
		System.out.println("Company name entered is: "+data);
	}

}
