package pomwithpagefactory;

import org.testleaf.leaftaps.base.ProjectBase;

public class MyHomePage extends ProjectBase {

	public MyLeadsPage clickLeads() {
    //Click on Leads button
		driver.findElementByXPath("//a[text()='Leads']").click();
		return new MyLeadsPage();

	}
}
