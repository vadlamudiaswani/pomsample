package pomwithpagefactory;

import org.testleaf.leaftaps.base.ProjectBase;
import org.testleaf.leaftaps.pages.HomePage;
import org.testleaf.leaftaps.pages.LoginPage;
import org.testng.annotations.Test;

public class TC002_CreateLead extends ProjectBase{
	@Test
	public void runCreateLead()
	{
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLoginButton()
		.verifyLoginName()
		.clickcrmsfaLink()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName()
		.enterFirstName()
		.enterLastName()
		.clickSubmit()
		.verifyFirstName();
	
		new HomePage()
		.logOut();
		
	}
	@Test
	public void runEditLead()
	{
		new LoginPage()
		.enterUserName()
		.enterPassword()
		.clickLoginButton()
		.verifyLoginName()
		.clickcrmsfaLink()
		.clickLeads()
		.clickFindLeads()
		.clickFirstLeadID()
		.clickEdit()
		.editCompanyName()
		.clickUpdate()
		.verifyCompanyName();
		
	}

}
