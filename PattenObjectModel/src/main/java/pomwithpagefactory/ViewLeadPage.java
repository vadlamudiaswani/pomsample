package pomwithpagefactory;

import org.testleaf.leaftaps.base.ProjectBase;

public class ViewLeadPage extends ProjectBase {

	public ViewLeadPage verifyFirstName() {
		String name = driver.findElementById("viewLead_firstName_sp").getText();
		if (name.contains(firstName)) {
			System.out.println("First Name Verified");
		} else {
			System.out.println("First not Verified");
		}
		return this;
	}


	// verifying Company name

	public ViewLeadPage verifyCompanyName() {
		String Name = driver.findElementById("viewLead_companyName_sp").getText();
		if (Name.substring(0, companyName.length()).equals(companyName)) {
			System.out.println("New Company Name is " + Name.substring(0, companyName.length())
					+ " Company name Matches : Update Successful");

		} else {
			System.out.println("Company Name not Matches Update Not Successful");
		}
		return this;
	}
}
